import { showMeSomething } from '../src';

describe('show me something', () => {
  it('should return some string', () => {
    expect(showMeSomething().length).toBeGreaterThan(0);
  });
});
