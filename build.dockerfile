FROM node:10-alpine

WORKDIR /opt/app
COPY src src
COPY package.json .
COPY yarn.lock .

RUN apk --update add --virtual build-dependencies tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && echo "Asia/Bangkok" > /etc/timezone \
    && date \
    && apk del build-dependencies \
    && yarn \
    && yarn build \
    && ls -lha

ENTRYPOINT [ "node", "dist/index.js" ]
