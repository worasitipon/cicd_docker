FROM node:10-alpine

WORKDIR /opt/app
COPY dist .

RUN apk --update add --virtual build-dependencies tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && echo "Asia/Bangkok" > /etc/timezone \
    && date \
    && apk del build-dependencies \
    && ls -lha

ENTRYPOINT [ "node", "index.js" ]
